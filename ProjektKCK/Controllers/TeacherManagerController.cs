﻿using Microsoft.AspNet.Identity;
using PagedList;
using ProjektKCK.Models;
using ProjektKCK.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ProjektKCK.Controllers
{
    /// <summary>
    /// Klasa kontrolera do obsługi uzytkownikow o roli prowadzacego.
    /// </summary>
    public class TeacherManagerController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        /// <summary>
        /// Zawiera przekierowanie do strony głownej panelu sterowania.
        /// </summary>
        /// <returns>Widok ze stroną startowa prowadzącego</returns>
        public ActionResult Index()
        {
            return RedirectToAction("Main", "TeacherManager");
        }

        /// <summary>
        /// Zawiera przekierowanie do strony głownej panelu sterowania.
        /// </summary>
        /// <returns>Widok ze stroną startowa prowadzącego</returns>
        public ActionResult Main()
        {
            return View();
        }

        /// <summary>
        /// Zawiera przekierowanie do strony edycji profilu.
        /// </summary>
        /// Metoda pobiera uzytkownika, rozpoznajac jego id oraz zwraca dane, ktore moze wyedytować.
        /// <returns>Widok z formularzem edycji profilu</returns>
        public ActionResult EditProfile()
        {
            var userID = User.Identity.GetUserId();
            var _userData = from n in db.UserDataModels where n.ApplicationUser.Id == userID select n;
            var userData = new UserDataModel();

            var _user = from x in db.Users where x.Id == userID select x;
            var user = _user.First();

            UserDataViewModel userDataViewModel = new UserDataViewModel();

            if (_userData.Count() == 1)
            {
                userData = _userData.First();
                userDataViewModel.Id = 1;
                userDataViewModel.UserName = user.UserName;
                userDataViewModel.Email = user.Email;
                userDataViewModel.Surname = userData.Surname;
                userDataViewModel.Address = userData.Address;
                userDataViewModel.Name = userData.Name;
                userDataViewModel.PhoneNumber = user.PhoneNumber;
            }
            else
            {
                userDataViewModel.Id = 1;
                userDataViewModel.UserName = user.UserName;
                userDataViewModel.Email = user.Email;
                userDataViewModel.Surname = "";
                userDataViewModel.Address = "";
                userDataViewModel.Name = "";
                userDataViewModel.PhoneNumber = user.PhoneNumber;
            }

            if (userDataViewModel == null)
            {
                return HttpNotFound();
            }
            return View(userDataViewModel);
        }

        /// <summary>
        /// Zapisuje zmiany uzytkownika w jego profilu.
        /// </summary>
        /// <param name='userDataViewModel'>egzemplarz klasy modelu "userDataViewModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>Widok z formularzem edycji profilu</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile([Bind(Include = "Id,UserName,Name,Email,Surname,Address,PhoneNumber")] UserDataViewModel userDataViewModel)
        {
            var userID = User.Identity.GetUserId();
            var _userData = from n in db.UserDataModels where n.ApplicationUser.Id == userID select n;
            var userData = new UserDataModel();
            var _user = from x in db.Users where x.Id == userID select x;
            var user = _user.First();


            if (_userData.Count() == 1)
            {
                userData = _userData.First();
            }
            else
            {
                userData.ApplicationUser = user;
                db.UserDataModels.Add(userData);
            }

            if (ModelState.IsValid)
            {
                user.PhoneNumber = userDataViewModel.PhoneNumber;
                user.Email = userDataViewModel.Email;
                userData.Address = userDataViewModel.Address;
                userData.Name = userDataViewModel.Name;
                userData.Surname = userDataViewModel.Surname;
                db.SaveChanges();
                return RedirectToAction("EditProfile");
            }
            return View(userDataViewModel);
        }

        /// <summary>
        /// zwraca strone z lista kursow ale tylko tych do ktorych dany nauczyciel jest przypisany jak prowadzacy.
        /// </summary>
        /// <param name='sortOrder'>String zawierajacy typ sortowania</param> 
        /// <param name='page'>Wybrana strona (zakladka) z wyswietlanymi kursami</param> 
        /// <returns>Lista kursow w systemie, ale tylko tych które prowadzi dany prowadzący</returns>
        public ActionResult ManageCourses(string sortOrder, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "language_desc" : "";
            ViewBag.DateSortParm = sortOrder == "date" ? "date_desc" : "date";
            var userID = User.Identity.GetUserId();
            var courses = from s in db.Courses where s.Teacher.Id == userID select s;

            switch (sortOrder)
            {
                case "language_desc":
                    courses = courses.OrderByDescending(s => s.Language);
                    break;
                case "date":
                    courses = courses.OrderBy(s => s.AddTime);
                    break;
                case "date_desc":
                    courses = courses.OrderByDescending(s => s.AddTime);
                    break;
                default:  // Name ascending 
                    courses = courses.OrderBy(s => s.Language);
                    break;
            }


            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(courses.ToPagedList(pageNumber, pageSize));

        }

        /// <summary>
        /// Zwraca widok na edycje konkretnego kursu
        /// </summary>
        /// <param name='id'>identyfikator wybranego kursu</param> 
        /// <returns>Widok formularza do edycji wybranego kursu</returns>
        public ActionResult EditCourse(string id)
        {
            int __id = Convert.ToInt32(id);
            //Debug.WriteLine(__id);
            var _aBase = from x in db.Courses where x.Id == __id select x;
            var aBase = _aBase.First();

            var aUsersCourses = (from x in db.MyCourses where x.CourseModel.Id == aBase.Id select x).ToList();

            var a = new CourseViewModel
            {
                Id = aBase.Id,
                Language = aBase.Language,
                Type = aBase.Type,
                AddTime = aBase.AddTime,
                Description = aBase.Description,
                Cost = aBase.Cost,
                Slots = aBase.Slots,
                ActivitiesList = aBase.ActivitiesList,
                CourseList = aUsersCourses           
                
            };

            ViewBag.TeacherID = new SelectList(db.Users, "Id", "UserName", aBase.Teacher.Id);

            if (aBase.Recruitment)
                ViewBag.RecID = new SelectList(new List<string> { "Otwarta", "Zamknięta" }, "", "", "Otwarta");
            else ViewBag.RecID = new SelectList(new List<string> { "Otwarta", "Zamknięta" }, "", "", "Zamknięta");

            if (aBase.Status)
                ViewBag.StatusID = new SelectList(new List<string> { "Trwa", "Zakonczony" }, "", "", "Trwa");
            else ViewBag.StatusID = new SelectList(new List<string> { "Trwa", "Zakonczony" }, "", "", "Zakonczony");


            return View(a);
        }

        /// <summary>
        /// Zatwierdza wprowadzone zmiany w kursie.
        /// </summary>
        /// <param name='courseModels'>egzemplarz klasy modelu "MyCourseModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>Lista kursow w systemie</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCourse([Bind(Include = "Id,Language,Type,AddTime,Description,Slots,Cost,Teacher,Recruitment,Status")] CourseViewModel courseModels)
        {

            var newCourse = (from x in db.Courses where courseModels.Id == x.Id select x).First();


            newCourse.Id = courseModels.Id;
            //newCourse.Language = courseModels.Language;
            newCourse.Type = courseModels.Type;
            newCourse.Description = courseModels.Description;
            //newCourse.Cost = courseModels.Cost;
            newCourse.Slots = courseModels.Slots;


            //newCourse.Teacher = (from x in db.Users where x.Id == courseModels.Teacher select x).First();
            if (courseModels.Recruitment == "Otwarta") { newCourse.Recruitment = true; } else newCourse.Recruitment = false;
            if (courseModels.Status == "Trwa") { newCourse.Status = true; } else newCourse.Status = false;
            db.SaveChanges();

            return RedirectToAction("ManageCourses");
        }

        /// <summary>
        /// Zwraca widok na edycje konkretnego uczestnika kursu.
        /// </summary>
        /// <param name='id'>identyfikator konkretneej instacji obiektu podkursu uzytkownika w danym kursie</param> 
        /// <returns>Widok na kurs pod kontem konkretego kursanta</returns>
        public ActionResult EditCourseUser(int id)
        {
            //int __id = Convert.ToInt32(id);
            //int __id2 = Convert.ToInt32(id2);
            //Debug.WriteLine(__id);
            //var _aBase = from x in db.Courses where x.Id == id select x;
            //var aBase = _aBase.First();

            //var aUserCourse = (from x in db.MyCourses where x.CourseModel.Id == aBase.Id && x.UserDataModel.Id == id2 select x).First();
            var aUserCourse = (from x in db.MyCourses where x.Id == id select x).First();

            ViewBag.ZalStat = new SelectList(new List<string> { "Brak", "Zaliczone", "NieZaliczone" }, "", "", aUserCourse.ZalStat);

            return View(aUserCourse);
        }


        /// <summary>
        /// Zatwierdza zmane statusu konkretnego kursatna - jego zaliczenia
        /// </summary>
        /// <param name='courseModels'>egzemplarz klasy modelu "MyCourseModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>Widok na kurs pod kontem konkretego kursanta</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCourseUser([Bind(Include = "Id,ZalStat")] MyCourseModel courseModels)
        {

            var _newCourse = from x in db.MyCourses where courseModels.Id == x.Id select x;

            var newCourse = _newCourse.First();

            newCourse.ZalStat = courseModels.ZalStat;
            db.SaveChanges();

            return Redirect("EditCourseUser?id="+ newCourse.Id);
        }

        /// <summary>
        /// Zwraca widok na liste ocen konkretnego kursanta w konkretnym kursie
        /// </summary>
        /// <param name='id'>identyfikator konkretnej oceny uzytkownika</param> 
        /// <param name='id2'>identyfikator niewykorzystywany</param> 
        /// <returns>Widok na formularz dodawania oceny dla konkretnego uzytkownika</returns>
        public ActionResult EditCourseUserGrade(int id, int id2)
        {
            //int __id = Convert.ToInt32(id);
            //int __id2 = Convert.ToInt32(id2);
            //Debug.WriteLine(__id);
            //var _aBase = from x in db.Courses where x.Id == id select x;
            //var aBase = _aBase.First();

            //var aUserCourse = (from x in db.MyCourses where x.CourseModel.Id == aBase.Id && x.UserDataModel.Id == id2 select x).First();
            var aUserCourse = (from x in db.MyCourses where x.Id == id select x).First();

            ViewBag.Grade = new SelectList(new List<int> { 2,3,4,5}, "", "", 2);

            return View(aUserCourse);
        }


        /// <summary>
        /// Zatwierdza nową ocene przyznana kursantowi w danym kursie
        /// </summary>
        /// <param name='courseModels'>egzemplarz klasy modelu "MyCourseModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>Widok na kurs pod kontem konkretego kursanta<</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCourseUserGrade([Bind(Include = "Id,LastGrade,LastGradeComment")] MyCourseModel courseModels)
        {

            var newCourse = (from x in db.MyCourses where courseModels.Id == x.Id select x).First();


            newCourse.Grades.Add(new GradeModel { Grade = courseModels.LastGrade, Comment = courseModels.LastGradeComment, MyCourseModel = newCourse });
            db.SaveChanges();

            return Redirect("EditCourseUser?id=" + newCourse.Id);
        }



        /// <summary>
        /// Zwraca widok z lista zajec dla konkretnego kursu.
        /// </summary>
        /// <param name='id'>identyfikator konkretnego podkursu uzytkownika w kursie</param> 
        /// <returns>Widok na formularz dodawania zajec dla konkretnego uzytkownika</returns>
        public ActionResult AddActivity(string id)
        {
            var dD = new List<int>();
            for (int i = 1;i <= 31;i++) dD.Add(i);

            var dM = new List<int>();
            for (int i = 1; i <= 12; i++) dM.Add(i);

            var dY = new List<int>();
            dY.Add(2016);
            dY.Add(2017);

            var tH = new List<int>();
            for (int i = 0; i <= 23; i++) tH.Add(i);

            var tM = new List<int>();
            for (int i = 0; i <= 59; i++) tM.Add(i);

            ViewBag.DateD = new SelectList(dD, "", "", 1);
            ViewBag.DateM = new SelectList(dM, "", "", 1);
            ViewBag.DateY = new SelectList(dY, "", "", 2016);

            ViewBag.TimeH = new SelectList(tH, "", "", 1);
            ViewBag.TimeM = new SelectList(tM, "", "", 1);

            Debug.WriteLine(id);
            int __id = Convert.ToInt32(id);
            Debug.WriteLine(__id);
            var cM = (from x in db.Courses where x.Id == __id select x).First();

            var aM= new ActivityViewModel { Id = cM.Id, CourseId = cM.Id };
         

            return View(aM);
        }

        /// <summary>
        /// Zatwierdza dodanie nowej lekcji w rozkladzie zajec dla konkretnego kursu.
        /// </summary>
        /// <param name='aM'>egzemplarz klasy modelu "ActivityViewModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>Widok na liste prowadzonych kursow<</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddActivity([Bind(Include = "Id,DateStartD,DateStartM,DateStartY,TimeStartH,TimeStartM,Duration,CourseId")] ActivityViewModel aM)
        {

            var cM = (from x in db.Courses where x.Id == aM.Id select x).First();

            var newAccv = new ActivityModel
            {
                DateStartD = aM.DateStartD,
                DateStartM = aM.DateStartM,
                DateStartY = aM.DateStartY,
                TimeStartH = aM.TimeStartH,
                TimeStartM = aM.TimeStartM,
                Duration = aM.Duration,
                CourseModel = cM
            };


            db.Activities.Add(newAccv);
            db.SaveChanges();

            return RedirectToAction("ManageCourses");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjektKCK.Models;
using ProjektKCK.ViewModels;
using System.Diagnostics;
using Microsoft.AspNet.Identity;
using PagedList;
using System.Web.Security;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ProjektKCK.Controllers
{
    /// <summary>
    /// Klasa kontrolera do obsługi uzytkownikow o roli admina.
    /// </summary>
    public class AdminManagerController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Zawiera przekierowanie do strony głownej panelu sterowania.
        /// </summary>
        /// <returns>Widok ze stroną startowa administratora</returns>
        public ActionResult Index()
        {
            return RedirectToAction("Main", "AdminManager");
        }

        /// <summary>
        /// Zawiera przekierowanie do strony głownej panelu sterowania.
        /// </summary>
        /// <returns>Widok ze stroną startowa administratora</returns>
        public ActionResult Main()
        {
            return View("Main");
        }

        /// <summary>
        /// Zawiera przekierowanie do strony edycji profilu.
        /// </summary>
        /// Metoda pobiera uzytkownika, rozpoznajac jego id oraz zwraca dane, ktore moze wyedytować.
        /// <returns>Widok z formularzem edycji profilu</returns>
        public ActionResult EditProfile()
        {
            var userID = User.Identity.GetUserId();
            var _userData = from n in db.UserDataModels where n.ApplicationUser.Id == userID select n;
            var userData = new UserDataModel();

            var _user = from x in db.Users where x.Id == userID select x;
            var user = _user.First();

            UserDataViewModel userDataViewModel = new UserDataViewModel();

            if (_userData.Count() == 1)
            {
                userData = _userData.First();
                userDataViewModel.Id = 1;
                userDataViewModel.UserName = user.UserName;
                userDataViewModel.Email = user.Email;
                userDataViewModel.Surname = userData.Surname;
                userDataViewModel.Address = userData.Address;
                userDataViewModel.Name = userData.Name;
                userDataViewModel.PhoneNumber = user.PhoneNumber;
            }
            else
            {


                userDataViewModel.Id = 1;
                userDataViewModel.UserName = user.UserName;
                userDataViewModel.Email = user.Email;
                userDataViewModel.Surname = "";
                userDataViewModel.Address = "";
                userDataViewModel.Name = "";
                userDataViewModel.PhoneNumber = user.PhoneNumber;
            }

            if (userDataViewModel == null)
            {
                return HttpNotFound();
            }
            return View(userDataViewModel);
        }

        /// <summary>
        /// Zapisuje zmiany uzytkownika w jego profilu.
        /// </summary>
        /// <param name='userDataViewModel'>egzemplarz klasy modelu "UserDataViewModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>Widok z formularzem edycji profilu</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile([Bind(Include = "Id,UserName,Name,Email,Surname,Address,PhoneNumber")] UserDataViewModel userDataViewModel)
        {
            var userID = User.Identity.GetUserId();
            var _userData = from n in db.UserDataModels where n.ApplicationUser.Id == userID select n;
            var userData = new UserDataModel();
            var _user = from x in db.Users where x.Id == userID select x;
            var user = _user.First();


            if (_userData.Count() == 1)
            {
                userData = _userData.First();
            }
            else
            {
                userData.ApplicationUser = user;
                db.UserDataModels.Add(userData);
            }



            if (ModelState.IsValid)
            {
                user.PhoneNumber = userDataViewModel.PhoneNumber;
                user.Email = userDataViewModel.Email;
                userData.Address = userDataViewModel.Address;
                userData.Name = userDataViewModel.Name;
                userData.Surname = userDataViewModel.Surname;
                db.SaveChanges();
                return RedirectToAction("EditProfile");
            }
            return View(userDataViewModel);
        }

        /// <summary>
        /// zwraca strone z lista kursow.
        /// </summary>
        /// <param name='sortOrder'>String zawierajacy typ sortowania</param> 
        /// <param name='page'>Wybrana strona (zakladka) z wyswietlanymi kursami</param> 
        /// <returns>Lista kursow w systemie</returns>
        public ActionResult ManageCourses(string sortOrder,int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "language_desc" : "";
            ViewBag.DateSortParm = sortOrder == "date" ? "date_desc" : "date";

            var courses = from s in db.Courses select s;

            switch (sortOrder)
            {
                case "language_desc":
                    courses = courses.OrderByDescending(s => s.Language);
                    break;
                case "date":
                    courses = courses.OrderBy(s => s.AddTime);
                    break;
                case "date_desc":
                    courses = courses.OrderByDescending(s => s.AddTime);
                    break;
                default:  // Name ascending 
                    courses = courses.OrderBy(s => s.Language);
                    break;
            }


            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(courses.ToPagedList(pageNumber, pageSize));

        }

        /// <summary>
        /// Zwraca widok z formularzem do stworzenia nowego kursu.
        /// </summary>
        /// <returns>Widok formularza do tworzenia nowego kursu</returns>
        public ActionResult CreateCourse()
        { 
            var a= new CourseViewModel();
            a.AddTime = DateTime.Now;
            ViewBag.TeacherID = new SelectList(db.Users, "Id", "UserName", 0);
            ViewBag.RecID = new SelectList(new List<string> { "Otwarta", "Zamknięta" }, "", "", 0);
            ViewBag.StatusID = new SelectList(new List<string> { "Trwa", "Zakonczony" }, "", "", 0);
            return View(a);
        }

        /// <summary>
        /// Zatwierdza stworzenie nowego kursu.
        /// </summary>
        /// <param name='courseModels'>egzemplarz klasy modelu "CourseViewModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>Lista kursow w systemie</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCourse([Bind(Include = "Id,Language,Type,AddTime,Description,Slots,Cost,Teacher,Recruitment,Status")] CourseViewModel courseModels)
        {
            var newCourse = new CourseModel {
                Language = courseModels.Language,
                Type = courseModels.Type,
                AddTime = DateTime.Now,
                Description = courseModels.Description,
                Cost = courseModels.Cost,
                Slots = courseModels.Slots,
            };

            newCourse.Teacher = (from x in db.Users where x.Id == courseModels.Teacher select x).First(); 
            if (courseModels.Recruitment == "Otwarta") { newCourse.Recruitment = true; } else newCourse.Recruitment = false;
            if (courseModels.Status == "Trwa") { newCourse.Status= true; } else newCourse.Status = false;

            db.Courses.Add(newCourse);
            db.SaveChanges();

            return RedirectToAction("ManageCourses");
        }

        /// <summary>
        /// Zwraca widok do ekycji konkretnego kursu.
        /// </summary>
        /// <param name='id'>identyfikator wybranego kursu</param> 
        /// <returns>Widok formularza do edycji wybranego kursu</returns>
        public ActionResult EditCourse(string id)
        {
            int __id = Convert.ToInt32(id);
            //Debug.WriteLine(__id);
            var _aBase = from x in db.Courses where x.Id == __id select x;
            var aBase = _aBase.First();
            var a = new CourseViewModel
            {
                Id = aBase.Id,
                Language = aBase.Language,
                Type = aBase.Type,
                AddTime = aBase.AddTime,
                Description = aBase.Description,
                Cost = aBase.Cost,
                Slots = aBase.Slots
            };

            ViewBag.TeacherID = new SelectList(db.Users, "Id", "UserName", aBase.Teacher.Id);

            if(aBase.Recruitment)
            ViewBag.RecID = new SelectList(new List<string> { "Otwarta", "Zamknięta" }, "", "", "Otwarta");
            else ViewBag.RecID = new SelectList(new List<string> { "Otwarta", "Zamknięta" }, "", "", "Zamknięta");

            if (aBase.Status)
                ViewBag.StatusID = new SelectList(new List<string> { "Trwa", "Zakonczony" }, "", "", "Trwa");
            else ViewBag.StatusID = new SelectList(new List<string> { "Trwa", "Zakonczony" }, "", "", "Zakonczony");


            return View(a);
        }

        /// <summary>
        /// Zwraca zmany w kursie.
        /// </summary>
        /// <param name='courseModels'>egzemplarz klasy modelu "CourseViewModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>Lista kursow w systemie</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCourse([Bind(Include = "Id,Language,Type,AddTime,Description,Slots,Cost,Teacher,Recruitment")] CourseViewModel courseModels)
        {

            var newCourse = (from x in db.Courses where courseModels.Id == x.Id select x).First();


            newCourse.Id = courseModels.Id;
            newCourse.Language = courseModels.Language;
            newCourse.Type = courseModels.Type;
            newCourse.Description = courseModels.Description;
            newCourse.Cost = courseModels.Cost;
            newCourse.Slots = courseModels.Slots;


            newCourse.Teacher = (from x in db.Users where x.Id == courseModels.Teacher select x).First();
            if (courseModels.Recruitment == "Otwarta") { newCourse.Recruitment = true; } else newCourse.Recruitment = false;

            db.SaveChanges();

            return RedirectToAction("ManageCourses");
        }

        /// <summary>
        /// Zwraca widok z lista uzytkownikow.
        /// </summary>
        /// <param name='sortOrder'>String zawierajacy typ sortowania</param> 
        /// <param name='page'>Wybrana strona (zakladka) z wyswietlanymi kursami</param> 
        /// <returns>Lista uzytkownikow w systemie</returns>
        public ActionResult ManageUsers(string sortOrder, int? page)
        {
            
            var objList = new List<UserManageViewModel>();
            var _numberOfUsers = (from x in db.Users select x).ToList();

            for(int i=0; i <_numberOfUsers.Count();i++)
            {
                var nObj = new UserManageViewModel();
                nObj.UserName = _numberOfUsers[i].UserName;
                nObj.Email = _numberOfUsers[i].Email;
                nObj.Id = _numberOfUsers[i].Id;


                Debug.WriteLine(_numberOfUsers[i].Id);

                string s = _numberOfUsers[i].Id;

                string r = _numberOfUsers[i].Roles.First().RoleId;

                var nObjData = from x in db.UserDataModels where x.ApplicationUser.Id == s select x;
                var nObjRoles = from x in db.Roles where x.Id == r select x;

                nObj.Role = nObjRoles.First().Name;

                if (nObjData.Count() > 0)
                {
                    nObj.Name = nObjData.First().Name;
                    nObj.Surname = nObjData.First().Surname;
                }
                else
                {
                    nObj.Name = "Nie uzupełniono";
                    nObj.Surname = "Nie uzupełniono";
                }

                objList.Add(nObj);
            }

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "email_desc" : "";
            ViewBag.DateSortParm = sortOrder == "username" ? "username_desc" : "username";

            var courses = objList as IEnumerable<UserManageViewModel>;

            switch (sortOrder)
            {
                case "username_desc":
                    courses = courses.OrderByDescending(s => s.UserName);
                    break;
                case "email":
                    courses = courses.OrderBy(s => s.Email);
                    break;
                case "email_desc":
                    courses = courses.OrderByDescending(s => s.Email);
                    break;
                default:  // Name ascending 
                    courses = courses.OrderBy(s => s.UserName);
                    break;
            }


            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(courses.ToPagedList(pageNumber, pageSize));

        }

        /// <summary>
        /// Edycja dla konkretnego uzytkownika.
        /// </summary>        
        /// <param name='id'>identyfikator uzytkownika</param> 
        /// <returns> Widok z edycja dla konkretnego uzytkownika</returns>
        public ActionResult EditUser(string id)
        {

            var user = (from x in db.Users where x.Id == id select x).First();

            var userData = (from x in db.UserDataModels where x.ApplicationUser.Id == id select x).First();

            var rID = user.Roles.First().RoleId;

            var userRoles = (from x in db.Roles where x.Id == rID select x.Name).First();


            var vmodel = new UserManageViewModel
            {
                UserName = user.UserName,
                Name = userData.Name,
                Surname = userData.Surname,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Address = userData.Address,
                Id = user.Id,
                Role = userRoles
            };

            ViewBag.RoleID = new SelectList(db.Roles, "Name", "Name", userRoles);




            return View(vmodel);
        }

        /// <summary>
        /// Zatwierdza zmiany wprowadzone dla konkretnego uzytkownika.
        /// </summary>
        /// <param name='userModels'>egzemplarz klasy modelu "UserManageViewModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>Lista uzytkownikow w systemie</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser([Bind(Include = "Id,Role")] UserManageViewModel userModels)
        {
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var user = (from x in db.Users where x.Id == userModels.Id select x).First();

            var userRoles = db.Roles.Include(r => r.Users).ToList();

            var userRoleName = (from r in userRoles
                                 from u in r.Users
                                 where u.UserId == user.Id
                                 select r.Name).ToList().First();


            if (userRoleName != userModels.Role)
            {
                user.Roles.Clear();
                UserManager.AddToRole(user.Id, userModels.Role);
                db.SaveChanges();
            }

            return RedirectToAction("ManageUsers");
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

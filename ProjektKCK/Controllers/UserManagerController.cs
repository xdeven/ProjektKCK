﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjektKCK.Models;
using ProjektKCK.ViewModels;
using System.Diagnostics;
using Microsoft.AspNet.Identity;
using PagedList;

namespace ProjektKCK.Controllers
{
    /// <summary>
    /// Klasa kontrolera do obsługi uzytkownikow o roli kursanta.
    /// </summary>
    public class UserManagerController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Zawiera przekierowanie do strony głownej panelu sterowania.
        /// </summary>
        /// <returns>Widok ze stroną startowa uzytkownika</returns>
        public ActionResult Index()
        {
            return RedirectToAction("Main", "UserManager");
        }

        /// <summary>
        /// Zawiera przekierowanie do strony głownej panelu sterowania.
        /// </summary>
        /// <returns>Widok ze stroną startowa uzytkownika</returns>
        public ActionResult Main()
        {
            return View();
        }

        /// <summary>
        /// Zawiera przekierowanie do strony edycji profilu.
        /// </summary>
        /// Metoda pobiera uzytkownika, rozpoznajac jego id oraz zwraca dane, ktore moze wyedytować.
        /// <returns>Widok z formularzem edycji profilu</returns>
        public ActionResult EditProfile()
        {
            var userID = User.Identity.GetUserId();
            var _userData = from n in db.UserDataModels where n.ApplicationUser.Id == userID select n;
            var userData = new UserDataModel();

            var _user = from x in db.Users where x.Id == userID select x;
            var user = _user.First();

            UserDataViewModel userDataViewModel = new UserDataViewModel();

            if (_userData.Count() == 1)
            {
                userData = _userData.First();
                userDataViewModel.Id = 1;
                userDataViewModel.UserName = user.UserName;
                userDataViewModel.Email = user.Email;
                userDataViewModel.Surname = userData.Surname;
                userDataViewModel.Address = userData.Address;
                userDataViewModel.Name = userData.Name;
                userDataViewModel.PhoneNumber = user.PhoneNumber;
            }
            else
            {
                userDataViewModel.Id = 1;
                userDataViewModel.UserName = user.UserName;
                userDataViewModel.Email = user.Email;
                userDataViewModel.Surname = "";
                userDataViewModel.Address = "";
                userDataViewModel.Name = "";
                userDataViewModel.PhoneNumber = user.PhoneNumber;
            }

            if (userDataViewModel == null)
            {
                return HttpNotFound();
            }
            return View(userDataViewModel);
        }

        /// <summary>
        /// Zapisuje zmiany uzytkownika w jego profilu.
        /// </summary>
        /// <param name='userDataViewModel'>egzemplarz klasy modelu "UserDataViewModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>Widok z formularzem edycji profilu</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile([Bind(Include = "Id,UserName,Name,Email,Surname,Address,PhoneNumber")] UserDataViewModel userDataViewModel)
        {
            var userID = User.Identity.GetUserId();
            var _userData = from n in db.UserDataModels where n.ApplicationUser.Id == userID select n;
            var userData = new UserDataModel();
            var _user = from x in db.Users where x.Id == userID select x;
            var user = _user.First();


            if (_userData.Count() == 1)
            {
                userData = _userData.First();
            }
            else
            {
                userData.ApplicationUser = user;
                db.UserDataModels.Add(userData);
            }

            if (ModelState.IsValid)
            {
                user.PhoneNumber = userDataViewModel.PhoneNumber;
                user.Email = userDataViewModel.Email;
                userData.Address = userDataViewModel.Address;
                userData.Name = userDataViewModel.Name;
                userData.Surname = userDataViewModel.Surname;
                db.SaveChanges();
                return RedirectToAction("EditProfile");
            }
            return View(userDataViewModel);
        }

        /// <summary>
        /// zwraca strone z lista kursow ale tylko tych do ktorych ktorych status rekrutacji jest otwarty!.
        /// </summary>
        /// <param name='sortOrder'>String zawierajacy typ sortowania</param> 
        /// <param name='page'>Wybrana strona (zakladka) z wyswietlanymi kursami</param> 
        /// <returns>Lista kursow w systemie</returns>
        public ActionResult ManageCourses(string sortOrder, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "language_desc" : "";
            ViewBag.DateSortParm = sortOrder == "date" ? "date_desc" : "date";

            var courses = from s in db.Courses where s.Status == true && s.Recruitment == true select s;

            switch (sortOrder)
            {
                case "language_desc":
                    courses = courses.OrderByDescending(s => s.Language);
                    break;
                case "date":
                    courses = courses.OrderBy(s => s.AddTime);
                    break;
                case "date_desc":
                    courses = courses.OrderByDescending(s => s.AddTime);
                    break;
                default:  // Name ascending 
                    courses = courses.OrderBy(s => s.Language);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(courses.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// zwraca widok na konkretny kurs - jego dane szczegolowe
        /// </summary>
        /// <param name='id'>identyfikator kursu</param> 
        /// <param name='success'>identyfikator konkretnego komunikatu, domuslnie =9</param> 
        /// <returns>szczegolowy opis wybranego kursu</returns>
        public ActionResult CourseDetails(string id, int ? success = 9)
        {

            int __id = Convert.ToInt32(id);
            var aBase = (from s in db.Courses where s.Id == __id select s).First();

           // var aBase2 = (from s in db.MyCourses where s.CourseModel.Id == aBase.Id select s).First();

            var aTeacher = (from s in db.UserDataModels where s.ApplicationUser.Id == aBase.Teacher.Id select s).Single();

            var a = new CourseViewModel
            {
                Id = aBase.Id,
                Language = aBase.Language,
                Type = aBase.Type,
                AddTime = aBase.AddTime,
                Description = aBase.Description,
                Cost = aBase.Cost,
                Slots = aBase.Slots,
                Teacher = aTeacher.Name + " " + aTeacher.Surname,
                SlotsTake = aBase.CourseList.Count().ToString()              
                
            };

            ViewBag.Success = success;
            return View(a);
        }

        /// <summary>
        /// Dodaje kursanta do kursu (jesli to mozliwe), jeśli nie mozliwe zwraca widok z odpowiednia informacja
        /// </summary>
        /// <param name='courseModels'>egzemplarz klasy modelu "CourseViewModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>szczegolowy opis wybranego kursu oraz ewentualny komuniakt dodatkowy</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CourseDetails([Bind(Include = "Id")] CourseViewModel courseModels)
        {
            int __id = Convert.ToInt32(courseModels.Id);
            var aBase = (from s in db.Courses where s.Id == __id select s).First();

            //pobieram swoja szczegolowa baze danych osobowych
            var userID = User.Identity.GetUserId();
            var _userData = from n in db.UserDataModels where n.ApplicationUser.Id == userID select n;

            if(_userData.Count() == 0) { return RedirectToAction("CourseDetails", new { id = courseModels.Id, success = 2 }); }

            var userData = _userData.First();

            //sprawdzam czy czasem juz nie jestem w tym kursie
            var check = from n in db.MyCourses where n.UserDataModel.Id == userData.Id && n.CourseModel.Id == __id select n;
            if (check.Count() > 0) { return RedirectToAction("CourseDetails", new { id = courseModels.Id, success = 0 }); }

            //sprawdzam czy aby nie ma juz pelnego obsadzenia kursu..
            if (aBase.CourseList.Count() >= aBase.Slots) { return RedirectToAction("CourseDetails", new { id = courseModels.Id, success = 3 }); }

                aBase.CourseList.Add(new MyCourseModel
                {
                    CourseModel = aBase,
                    UserDataModel = userData,
                    PayKey = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 6),
                    ZalStat ="Brak",
                    Payed = false
                });
                db.SaveChanges();

            return RedirectToAction("CourseDetails", new { id = courseModels.Id, success = 1 });
        }

        /// <summary>
        /// Wykonuje rezygnacje ze wskazanego kursu
        /// </summary>
        /// <param name='id'>identyfikator kursu</param> 
        /// <returns>lista kursow do ktorych zapisany jest uzytkownik w systemie</returns>
        public ActionResult Resignation(string id)
        {
            int __id = Convert.ToInt32(id);
            var _aBase = from s in db.MyCourses where s.Id == __id select s;
            Debug.WriteLine(_aBase.First().PayKey);
            if (_aBase.Count() > 0)
            {
                var aBase = _aBase.First();
                var userID = User.Identity.GetUserId();
                if (_aBase.First().CourseModel.Recruitment && _aBase.First().UserDataModel.ApplicationUser.Id == userID)
                {
                    db.MyCourses.Remove(_aBase.First());
                    db.SaveChanges();
                }
            }

            return RedirectToAction("ManageMyCourses");
        }

        /// <summary>
        /// Zwraca widok na liste kursow do ktorych nalezy kursant
        /// </summary>
        /// <param name='sortOrder'>String zawierajacy typ sortowania</param> 
        /// <param name='page'>Wybrana strona (zakladka) z wyswietlanymi kursami</param> 
        /// <returns>lista kursow do ktorych zapisany jest uzytkownik w systemie</returns>
        public ActionResult ManageMyCourses(string sortOrder, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.ActiveSortParm = String.IsNullOrEmpty(sortOrder) ? "active_desc" : "";
            ViewBag.DateSortParm = sortOrder == "date" ? "date_desc" : "date";

            var userID = User.Identity.GetUserId();

            var courses = from s in db.MyCourses where s.UserDataModel.ApplicationUser.Id == userID select s;

            switch (sortOrder)
            {
                case "active_desc":
                    courses = courses.OrderByDescending(s => s.CourseModel.Status);
                    break;
                case "date":
                    courses = courses.OrderBy(s => s.CourseModel.AddTime);
                    break;
                case "date_desc":
                    courses = courses.OrderByDescending(s => s.CourseModel.AddTime);
                    break;
                default:  // Name ascending 
                    courses = courses.OrderBy(s => s.CourseModel.Status);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(courses.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Zwraca widok na konkretnie wybrany kurs do ktorego nalezy uzytkownik
        /// </summary>
        /// <param name='id'>identyfikator kursu</param> 
        /// <param name='success'>identyfikator konkretnego komunikatu, domuslnie =9</param> 
        /// <returns>szczegolowy opis wybranego kursu, w ktyrym udzial bierze uzytkownik</returns>
        public ActionResult MyCourseDetails(string id, int? success = 9)
        {
            var userID = User.Identity.GetUserId();
            int __id = Convert.ToInt32(id);
            var aBase = (from s in db.MyCourses where s.Id == __id select s).First();

            var aTeacher = (from s in db.UserDataModels where s.ApplicationUser.Id == aBase.CourseModel.Teacher.Id select s).Single();

            var a = new MyCourseViewModel
            {
                Id = aBase.Id,
                Language = aBase.CourseModel.Language,
                Type = aBase.CourseModel.Type,
                AddTime = aBase.CourseModel.AddTime,
                Description = aBase.CourseModel.Description,
                Cost = aBase.CourseModel.Cost,
                Slots = aBase.CourseModel.Slots,
                Teacher = aTeacher.Name + " " + aTeacher.Surname,
                SlotsTake = aBase.CourseModel.CourseList.Count().ToString(),
                ZalStat = aBase.ZalStat,
                MyCourseModel = aBase,
                Comment = aBase.Comment
            };

            if (aBase.CourseModel.Status) a.Status = "Trwa"; else a.Status = "Zakończony";
            if (aBase.CourseModel.Recruitment) a.Recruitment = "Otwarta"; else a.Recruitment = "Zakończona";
            a.Payed = aBase.Payed;
            a.PayKey = aBase.PayKey;

            ViewBag.Success = success;
            return View(a);
        }

        /// <summary>
        /// Zatwierdza dodany przez uzytkownika komentarz
        /// </summary>
        /// <param name='courseModels'>egzemplarz klasy modelu "MyCourseViewModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>szczegolowy opis wybranego kursu, w ktyrym udzial bierze uzytkownik</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MyCourseDetails([Bind(Include = "Id,Comment")] MyCourseViewModel courseModels)
        {
            var aBase = (from s in db.MyCourses where s.Id == courseModels.Id select s).First();

            aBase.Comment = courseModels.Comment;
            db.SaveChanges();
            return Redirect("../MyCourseDetails/"+courseModels.Id);
        }

        /// <summary>
        /// Zwraca widok na liste platnosci uzytkownika
        /// </summary>
        /// <param name='sortOrder'>String zawierajacy typ sortowania</param> 
        /// <param name='page'>Wybrana strona (zakladka) z wyswietlanymi kursami</param> 
        /// <returns>lista zatwierdzonych platnosci przez konkretnego uzytkownika</returns>
        public ActionResult ManagePayments(string sortOrder, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.ActiveSortParm = String.IsNullOrEmpty(sortOrder) ? "active_desc" : "";
            ViewBag.DateSortParm = sortOrder == "date" ? "date_desc" : "date";

            var userID = User.Identity.GetUserId();

            var courses = from s in db.Payments where s.UserDataModel.ApplicationUser.Id == userID select s;

            switch (sortOrder)
            {
                case "active_desc":
                    courses = courses.OrderByDescending(s => s.PayKey);
                    break;
                case "date":
                    courses = courses.OrderBy(s => s.CourseModel.AddTime);
                    break;
                case "date_desc":
                    courses = courses.OrderByDescending(s => s.CourseModel.AddTime);
                    break;
                default:
                    courses = courses.OrderBy(s => s.PayKey);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(courses.ToPagedList(pageNumber, pageSize));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjektKCK.Models;
using ProjektKCK.ViewModels;
using System.Diagnostics;
using Microsoft.AspNet.Identity;
using PagedList;
using System.Web.Security;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ProjektKCK.Controllers
{
    /// <summary>
    /// Klasa kontrolera do obsługi uzytkownikow o roli ksiegowego.
    /// </summary>

    public class AccountantManagerController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        /// <summary>
        /// Zawiera przekierowanie do strony głownej panelu sterowania.
        /// </summary>
        /// <returns>Widok ze stroną startowa ksiegowego</returns>
        public ActionResult Index()
        {
            return RedirectToAction("Main", "AccountantManager");
        }


        /// <summary>
        /// Zawiera przekierowanie do strony głownej panelu sterowania.
        /// </summary>
        /// <returns>Widok ze stroną startowa ksiegowego</returns>
        public ActionResult Main()
        {
            return View();
        }

        /// <summary>
        /// Zawiera przekierowanie do strony edycji profilu.
        /// </summary>
        /// Metoda pobiera uzytkownika, rozpoznajac jego id oraz zwraca dane, ktore moze wyedytować.
        /// <returns>Widok z formularzem edycji profilu</returns>
        public ActionResult EditProfile()
        {
            var userID = User.Identity.GetUserId();
            var _userData = from n in db.UserDataModels where n.ApplicationUser.Id == userID select n;
            var userData = new UserDataModel();

            var _user = from x in db.Users where x.Id == userID select x;
            var user = _user.First();

            UserDataViewModel userDataViewModel = new UserDataViewModel();

            if (_userData.Count() == 1)
            {
                userData = _userData.First();
                userDataViewModel.Id = 1;
                userDataViewModel.UserName = user.UserName;
                userDataViewModel.Email = user.Email;
                userDataViewModel.Surname = userData.Surname;
                userDataViewModel.Address = userData.Address;
                userDataViewModel.Name = userData.Name;
                userDataViewModel.PhoneNumber = user.PhoneNumber;
            }
            else
            {


                userDataViewModel.Id = 1;
                userDataViewModel.UserName = user.UserName;
                userDataViewModel.Email = user.Email;
                userDataViewModel.Surname = "";
                userDataViewModel.Address = "";
                userDataViewModel.Name = "";
                userDataViewModel.PhoneNumber = user.PhoneNumber;
            }

            if (userDataViewModel == null)
            {
                return HttpNotFound();
            }
            return View(userDataViewModel);
        }

        /// <summary>
        /// Zapisuje zmiany uzytkownika w jego profilu.
        /// </summary>
        /// <param name='userDataViewModel'>egzemplarz klasy modelu "userDataViewModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>Widok z formularzem edycji profilu</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile([Bind(Include = "Id,UserName,Name,Email,Surname,Address,PhoneNumber")] UserDataViewModel userDataViewModel)
        {
            var userID = User.Identity.GetUserId();
            var _userData = from n in db.UserDataModels where n.ApplicationUser.Id == userID select n;
            var userData = new UserDataModel();
            var _user = from x in db.Users where x.Id == userID select x;
            var user = _user.First();


            if (_userData.Count() == 1)
            {
                userData = _userData.First();
            }
            else
            {
                userData.ApplicationUser = user;
                db.UserDataModels.Add(userData);
            }



            if (ModelState.IsValid)
            {
                user.PhoneNumber = userDataViewModel.PhoneNumber;
                user.Email = userDataViewModel.Email;
                userData.Address = userDataViewModel.Address;
                userData.Name = userDataViewModel.Name;
                userData.Surname = userDataViewModel.Surname;
                db.SaveChanges();
                return RedirectToAction("EditProfile");
            }
            return View(userDataViewModel);
        }

        /// <summary>
        /// Zwraca widok na formularz do ksiegowania platnosci
        /// </summary>
        /// <param name='arg'>Elentualny kod bledu, domyslnie brak bledu czyli 0</param> 
        /// <returns>Widok z formularzem do akceptacji platnosci</returns>
        public ActionResult AddPayment(int ? arg = 0 )
        {
            return View(new AddPaymentViewModel());
        }


        /// <summary>
        /// Zatwierdza platnosc
        /// </summary>
        /// <param name='payViewModel'>egzemplarz klasy modelu "payViewModel", ze z'bind'owanymi wlasciwosciami</param> 
        /// <returns>Widok z formularzem do akceptacji platnosci</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPayment([Bind(Include = "Name,Surname,PayKey,Money")] AddPaymentViewModel payViewModel)
        {

            //sprawdzam czy istnieje juz taka wplata
            var _checkPay = from x in db.Payments where x.PayKey == payViewModel.PayKey select x;

            if (_checkPay.Count() > 0) { payViewModel.arg = 5; return View(payViewModel); } //wplata juz istnieje

            //sprawdzam czy istnieje taki mycourse
            var _myCourse = from x in db.MyCourses where x.PayKey == payViewModel.PayKey select x;

            if (_myCourse.Count() > 0)
            {
                var myCourse = _myCourse.First();
                if (myCourse.CourseModel.Cost == payViewModel.Money)
                {
                    if (myCourse.UserDataModel.Name == payViewModel.Name && myCourse.UserDataModel.Surname == payViewModel.Surname)
                    {
                        if (ModelState.IsValid)
                        {
                            var payModel = new PaymentModel();
                            payModel.Name = payViewModel.Name;
                            payModel.Surname = payViewModel.Surname;
                            payModel.PayKey = payViewModel.PayKey;
                            payModel.Money = payViewModel.Money;
                            payModel.CourseModel = myCourse.CourseModel;
                            payModel.UserDataModel = myCourse.UserDataModel;
                            db.Payments.Add(payModel);
                            myCourse.Payed = true;
                            myCourse.PaymentModel = payModel;
                            db.Payments.Add(payModel);
                            db.SaveChanges();
                            payViewModel.arg = 6; return View(payViewModel);//wszystko ok

                        }
                        else { payViewModel.arg = 4; return View(payViewModel); } //zle dane osobowe
                    }
                    else { payViewModel.arg = 3; return View(payViewModel); } //zle dane osobowe
                }
                else { payViewModel.arg = 2; return View(payViewModel); } //zla suma wplaty

            }
            else { payViewModel.arg = 1; return View(payViewModel); }//zly paykey

        }

        /// <summary>
        /// zwraca strone z lista kursow.
        /// </summary>
        /// <param name='sortOrder'>String zawierajacy typ sortowania</param> 
        /// <param name='page'>Wybrana strona (zakladka) z wyswietlanymi kursami</param> 
        /// <returns>Lista kursow w systemie</returns>
        public ActionResult ManageCourses(string sortOrder, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "language_desc" : "";
            ViewBag.DateSortParm = sortOrder == "date" ? "date_desc" : "date";

            var courses = from s in db.Courses select s;

            switch (sortOrder)
            {
                case "language_desc":
                    courses = courses.OrderByDescending(s => s.Language);
                    break;
                case "date":
                    courses = courses.OrderBy(s => s.AddTime);
                    break;
                case "date_desc":
                    courses = courses.OrderByDescending(s => s.AddTime);
                    break;
                default:  // Name ascending 
                    courses = courses.OrderBy(s => s.Language);
                    break;
            }


            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(courses.ToPagedList(pageNumber, pageSize));

        }

        /// <summary>
        /// Zwraca widok na konkretnie wybrany kurs
        /// </summary>
        /// <param name='id'>identyfikator konkretnego kursu</param> 
        /// <returns>Lista "CourseStatViewModel", czyli lista instancji obiektów modelu kursow dla uzytkownikow zapisanych do kursu</returns>
        public ActionResult CheckCourse(string id)
        {
            int __id = Convert.ToInt32(id);

            var courses = (from x in db.MyCourses where x.CourseModel.Id == __id select x).ToList();

            var lst = new List<CourseStatViewModel>();

            for(int i =0;i<courses.Count();i++)
            {
                lst.Add( new CourseStatViewModel { Id = courses[i].Id, Name = courses[i].UserDataModel.Name, Surname = courses[i].UserDataModel.Surname, Payed = courses[i].Payed } );
            }

            return View(lst);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

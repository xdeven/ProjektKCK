﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ProjektKCK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ProjektKCK.Controllers
{
    /// <summary>
    /// Klasa kontrolera do obsługi niezalogowanych uzytkowników.
    /// </summary>

    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Zwraca index do odpowiedniego panelu lub strony glownej.
        /// </summary>
        /// <returns>Widok ze strona glowna (niezalogowani) lub widok panelu uzytkownika w zaleznosci od roli uzytkownika systemu</returns>
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var userStore = new UserStore<ApplicationUser>(db);
                var userManager = new UserManager<ApplicationUser>(userStore);

                var us = User.Identity.GetUserId();
               // var user = userManager.FindById(us);

                

                if( userManager.IsInRole(us, "Administrator"))
                {
                    return RedirectToAction("Index", "AdminManager");
                }else
                if (userManager.IsInRole(us, "Ksiegowa"))
                {
                    return RedirectToAction("Index", "AccountantManager");
                }else
                if (userManager.IsInRole(us, "Prowadzacy"))
                {
                    return RedirectToAction("Index", "TeacherManager");
                }else
                if (userManager.IsInRole(us, "Kursant"))
                {
                    return RedirectToAction("Index", "UserManager");
                }else return View();

            }
            else return View();
        }

    }
}
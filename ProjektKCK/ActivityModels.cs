//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjektKCK
{
    using System;
    using System.Collections.Generic;
    
    public partial class ActivityModels
    {
        public int Id { get; set; }
        public int DateStartD { get; set; }
        public int DateStartM { get; set; }
        public int DateStartY { get; set; }
        public int TimeStartH { get; set; }
        public int TimeStartM { get; set; }
        public int Duration { get; set; }
        public Nullable<int> CourseModel_Id { get; set; }
    
        public virtual CourseModels CourseModels { get; set; }
    }
}

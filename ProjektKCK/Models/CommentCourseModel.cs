﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektKCK.Models
{
    public class CommentCourseModel
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public virtual MyCourseModel MyCourseModel { get; set; }
        public virtual UserDataModel UserDataModel { get; set; }
    }
}
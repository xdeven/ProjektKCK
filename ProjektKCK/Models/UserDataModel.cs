﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektKCK.Models
{
    public class UserDataModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

    }
}
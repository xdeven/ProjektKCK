﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektKCK.Models
{
    public class PaymentModel
    {
        public int Id { get; set; }
        public string PayKey { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Money { get; set; }
        public virtual CourseModel CourseModel { get; set; }
        public virtual UserDataModel UserDataModel { get; set; }
    }
}
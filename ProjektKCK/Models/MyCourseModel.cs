﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektKCK.Models
{
    public class MyCourseModel
    {
        public int Id { get; set; }
        public string PayKey { get; set; }
        public bool Payed { get; set; }
        public string ZalStat { get; set; }
        public int LastGrade { get; set; }
        public string LastGradeComment { get; set; }
        public string Comment { get; set; }
        public virtual CourseModel CourseModel { get; set; }
        public virtual UserDataModel UserDataModel { get; set; }
        public virtual PaymentModel PaymentModel { get; set; }
        public virtual ICollection<GradeModel> Grades { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektKCK.Models
{
    public class GradeModel
    {
        public int Id { get; set; }
        public int Grade { get; set; }
        public string Comment { get; set; }
        public virtual MyCourseModel MyCourseModel { get; set;}
    }
}
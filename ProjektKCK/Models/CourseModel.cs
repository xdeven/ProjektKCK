﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektKCK.Models
{
    public class CourseModel
    {
        public int Id { get; set; }
        public string Language { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public int Slots { get; set; }
        public string Cost { get; set; }
        public bool Recruitment { get; set; }
        public bool Status { get; set; }
        public DateTime AddTime { get; set; }
        public virtual ApplicationUser Teacher { get; set; }
        public virtual ICollection<MyCourseModel> CourseList {get; set;}
        public virtual ICollection<ActivityModel> ActivitiesList { get; set; }
    }
}
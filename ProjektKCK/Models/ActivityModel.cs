﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektKCK.Models
{
    public class ActivityModel
    {
        public int Id { get; set; }
        public int DateStartD { get; set; }
        public int DateStartM { get; set; }
        public int DateStartY { get; set; }
        public int TimeStartH { get; set; }
        public int TimeStartM { get; set; }
        public int Duration { get; set; }
        public virtual CourseModel CourseModel { get; set; }
    }
}
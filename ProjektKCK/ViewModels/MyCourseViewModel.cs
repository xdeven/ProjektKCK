﻿using ProjektKCK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektKCK.ViewModels
{
    public class MyCourseViewModel
    {
        public int Id { get; set; }
        public string Language { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public int Slots { get; set; }
        public string Cost { get; set; }
        public string Recruitment { get; set; }
        public string Status { get; set; }
        public DateTime AddTime { get; set; }
        public string Teacher { get; set; }
        public string SlotsTake { get; set; }
        public string PayKey { get; set; }
        public bool Payed { get; set; }
        public string ZalStat { get; set; }
        public MyCourseModel MyCourseModel { get; set; }
        public string Comment { get; set; }
    }
}
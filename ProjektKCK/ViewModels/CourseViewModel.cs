﻿using ProjektKCK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektKCK.ViewModels
{
    public class CourseViewModel
    {
        public int Id { get; set; }
        public string Language { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public int Slots { get; set; }
        public string Cost { get; set; }
        public string Recruitment { get; set; }
        public string Status { get; set; }
        public DateTime AddTime { get; set; }
        public string Teacher { get; set; }
        public string SlotsTake { get; set; }
        public ICollection<ActivityModel> ActivitiesList { get; set; }
        public ICollection<MyCourseModel> CourseList { get; set; }
    }
}
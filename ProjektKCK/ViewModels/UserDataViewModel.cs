﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektKCK.ViewModels
{
    public class UserDataViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Nazwa użytkownika")]
        [StringLength(20, ErrorMessage = "Nazwa użytkownika musi zawierac od 6 do 20 znaków", MinimumLength = 5)]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Imie")]
        [StringLength(20, ErrorMessage = "Imie uzytkownika musi zawierac od 6 do 20 znaków", MinimumLength = 1)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Adres Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Nazwisko")]
        [StringLength(20, ErrorMessage = "Nazwisko użytkownika musi zawierac od 6 do 20 znaków", MinimumLength = 1)]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "Adres Zamieszkania")]
        [StringLength(20, ErrorMessage = "Adres Zamieszkania musi zawierac od 6 do 20 znaków", MinimumLength = 3)]
        public string Address { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Numer telefonu")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
    }
}
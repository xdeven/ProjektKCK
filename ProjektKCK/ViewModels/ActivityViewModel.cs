﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektKCK.Models
{
    public class ActivityViewModel
    {
        public int Id { get; set; }
        public int DateStartD { get; set; }
        public int DateStartM { get; set; }
        public int DateStartY { get; set; }
        public int TimeStartH { get; set; }
        public int TimeStartM { get; set; }
        public int Duration { get; set; }
        public int CourseId { get; set; }
    }
}
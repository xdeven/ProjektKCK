﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektKCK.ViewModels
{
    public class CourseStatViewModel
    {
        public int Id { get; set; }
        public bool Payed { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

    }
}
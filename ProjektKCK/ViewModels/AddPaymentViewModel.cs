﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektKCK.ViewModels
{
    public class AddPaymentViewModel
    {
        public int arg {get;set;}
        public string PayKey { get; set; }
        public string Money { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
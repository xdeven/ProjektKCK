﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using ProjektKCK.Models;

[assembly: OwinStartupAttribute(typeof(ProjektKCK.Startup))]
namespace ProjektKCK
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
        }
        private void createRolesandUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (!roleManager.RoleExists("Administrator"))
            { //admin
                var role = new IdentityRole();
                role.Name = "Administrator";
                roleManager.Create(role);
				
                var user = new ApplicationUser();
                user.UserName = "admin";
                user.Email = "admin@admin.com";

                string userPWD = "123123";
                var chkUser = UserManager.Create(user, userPWD);
                var result1 = UserManager.AddToRole(user.Id, "Administrator");
            }
            if (!roleManager.RoleExists("Kursant"))
            { // kursant
                var role = new IdentityRole();
                role.Name = "Kursant";
                roleManager.Create(role);
            }
            if (!roleManager.RoleExists("Ksiegowa"))
            {// ksiegowa
                var role = new IdentityRole();
                role.Name = "Ksiegowa";
                roleManager.Create(role);
            }
            if (!roleManager.RoleExists("Prowadzacy"))
            {//prowadzacy
                var role = new IdentityRole();
                role.Name = "Prowadzacy";
                roleManager.Create(role);

            }

        }
    }
}

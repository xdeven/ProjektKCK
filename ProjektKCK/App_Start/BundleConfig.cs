﻿using System.Web;
using System.Web.Optimization;

namespace ProjektKCK
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            //frontpage
            bundles.Add(new ScriptBundle("~/bundles/bootstrap/frontpage").Include(
                      "~/Scripts/frontpage/js/bootstrap.min.js",
                      "~/Scripts/frontpage/js/jquery.inview.min.js",
                      "~/Scripts/frontpage/js/wow.min.js",
                      "~/Scripts/frontpage/js/mousescroll.js",
                      "~/Scripts/frontpage/js/smoothscroll.js",
                      "~/Scripts/frontpage/js/jquery.countTo.js",
                      "~/Scripts/frontpage/js/lightbox.min.js",
                      "~/Scripts/frontpage/js/main.js",
                      "~/Scripts/frontpage/respond.js"));

            bundles.Add(new StyleBundle("~/ContentCssFrontpage").Include(
                      "~/Content/frontpage/css/bootstrap.min.css",
                      "~/Content/frontpage/css/animate.min.css",
                      "~/Content/frontpage/css/font-awesome.min.css",
                      "~/Content/frontpage/css/lightbox.css",
                      "~/Content/frontpage/css/main.css",
                      "~/Content/frontpage/css/responsive.css"));

            //backpage
            bundles.Add(new ScriptBundle("~/bundles/bootstrap/backpage").Include(
                      "~/Scripts/backpage/js/jquery-1.10.2.js",
                      "~/Scripts/backpage/js/bootstrap.min.js",
                      "~/Scripts/backpage/js/bootstrap-checkbox-radio-switch.js",
                      "~/Scripts/backpage/js/chartist.min.js",
                      "~/Scripts/backpage/js/bootstrap-notify.js",
                      "~/Scripts/backpage/js/light-bootstrap-dashboard.js"));

            bundles.Add(new StyleBundle("~/ContentCssBackpage").Include(
                      "~/Content/backpage/css/bootstrap.min.css",
                      "~/Content/backpage/css/animate.min.css",
                      "~/Content/backpage/css/light-bootstrap-dashboard.css",
                      "~/Content/backpage/css/pe-icon-7-stroke.css",
                      "~/Content/backpage/css/font-awesome.min.css"));

        }
    }
}

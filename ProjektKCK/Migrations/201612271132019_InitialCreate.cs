namespace ProjektKCK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivityModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateStartD = c.Int(nullable: false),
                        DateStartM = c.Int(nullable: false),
                        DateStartY = c.Int(nullable: false),
                        TimeStartH = c.Int(nullable: false),
                        TimeStartM = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                        CourseModel_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CourseModels", t => t.CourseModel_Id)
                .Index(t => t.CourseModel_Id);
            
            CreateTable(
                "dbo.CourseModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Language = c.String(),
                        Type = c.String(),
                        Description = c.String(),
                        Slots = c.Int(nullable: false),
                        Cost = c.String(),
                        Recruitment = c.Boolean(nullable: false),
                        Status = c.Boolean(nullable: false),
                        AddTime = c.DateTime(nullable: false),
                        Teacher_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Teacher_Id)
                .Index(t => t.Teacher_Id);
            
            CreateTable(
                "dbo.MyCourseModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PayKey = c.String(),
                        Payed = c.Boolean(nullable: false),
                        ZalStat = c.String(),
                        CourseModel_Id = c.Int(),
                        PaymentModel_Id = c.Int(),
                        UserDataModel_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CourseModels", t => t.CourseModel_Id)
                .ForeignKey("dbo.PaymentModels", t => t.PaymentModel_Id)
                .ForeignKey("dbo.UserDataModels", t => t.UserDataModel_Id)
                .Index(t => t.CourseModel_Id)
                .Index(t => t.PaymentModel_Id)
                .Index(t => t.UserDataModel_Id);
            
            CreateTable(
                "dbo.GradeModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Grade = c.Int(nullable: false),
                        Comment = c.String(),
                        MyCourseModel_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MyCourseModels", t => t.MyCourseModel_Id)
                .Index(t => t.MyCourseModel_Id);
            
            CreateTable(
                "dbo.PaymentModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PayKey = c.String(),
                        Name = c.String(),
                        Surname = c.String(),
                        Money = c.String(),
                        CourseModel_Id = c.Int(),
                        UserDataModel_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CourseModels", t => t.CourseModel_Id)
                .ForeignKey("dbo.UserDataModels", t => t.UserDataModel_Id)
                .Index(t => t.CourseModel_Id)
                .Index(t => t.UserDataModel_Id);
            
            CreateTable(
                "dbo.UserDataModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        Address = c.String(),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.CourseModels", "Teacher_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.MyCourseModels", "UserDataModel_Id", "dbo.UserDataModels");
            DropForeignKey("dbo.MyCourseModels", "PaymentModel_Id", "dbo.PaymentModels");
            DropForeignKey("dbo.PaymentModels", "UserDataModel_Id", "dbo.UserDataModels");
            DropForeignKey("dbo.UserDataModels", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PaymentModels", "CourseModel_Id", "dbo.CourseModels");
            DropForeignKey("dbo.GradeModels", "MyCourseModel_Id", "dbo.MyCourseModels");
            DropForeignKey("dbo.MyCourseModels", "CourseModel_Id", "dbo.CourseModels");
            DropForeignKey("dbo.ActivityModels", "CourseModel_Id", "dbo.CourseModels");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.UserDataModels", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.PaymentModels", new[] { "UserDataModel_Id" });
            DropIndex("dbo.PaymentModels", new[] { "CourseModel_Id" });
            DropIndex("dbo.GradeModels", new[] { "MyCourseModel_Id" });
            DropIndex("dbo.MyCourseModels", new[] { "UserDataModel_Id" });
            DropIndex("dbo.MyCourseModels", new[] { "PaymentModel_Id" });
            DropIndex("dbo.MyCourseModels", new[] { "CourseModel_Id" });
            DropIndex("dbo.CourseModels", new[] { "Teacher_Id" });
            DropIndex("dbo.ActivityModels", new[] { "CourseModel_Id" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.UserDataModels");
            DropTable("dbo.PaymentModels");
            DropTable("dbo.GradeModels");
            DropTable("dbo.MyCourseModels");
            DropTable("dbo.CourseModels");
            DropTable("dbo.ActivityModels");
        }
    }
}

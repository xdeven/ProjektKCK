﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjektKCK.Controllers;
using ProjektKCK.Models;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestProject1
{
    [TestClass]
    public class TestyInzynieria
    {
        [TestMethod]
        public void Test1()
        {

            UserDataModel uM = new UserDataModel()
            {
                Name = "Adam",Surname = "Nowak", Address = "Warszawa"
            };

            CourseModel c1 = new CourseModel(){
                Language = "Angielski", Cost = "800", AddTime = DateTime.Now,Recruitment = true,Status = true,Type = "Stacionarne",
                Description = "Najlepszy kurs",Slots = 10,CourseList = new List<MyCourseModel>()
            };

            CourseModel c2 = new CourseModel()
            {
                Language = "Niemiecki",Cost = "300",AddTime = DateTime.Now,Recruitment = true,Status = true,Type = "Stacionarne",
                Description = "Najlepszy Niemiecki",Slots = 5,CourseList = new List<MyCourseModel>()
            };

            MyCourseModel m1 = new MyCourseModel(){
                CourseModel = c1,ZalStat = "zal",Comment = "",Payed = false,PayKey = "",UserDataModel = uM, Grades = new List<GradeModel>()
            };
            MyCourseModel m2 = new MyCourseModel(){
                CourseModel = c1,ZalStat = "zal",Comment = "",Payed = false,PayKey = "",UserDataModel = uM, Grades = new List<GradeModel>()
            };

            m1.UserDataModel = uM;
            m2.UserDataModel = uM;

            c1.CourseList.Add(m1);
            c1.CourseList.Add(m2);

            m1.Grades.Add(new GradeModel() { Grade = 4 });
            m1.Grades.Add(new GradeModel() { Grade = 2 });
            m1.Grades.Add(new GradeModel() { Grade = 5 });


            Assert.AreEqual("Stacionarne", c1.Type);// poprawnosc danych w modelu
            Assert.AreEqual(10, c1.Slots);// poprawnosc danych w modelu

            Assert.AreEqual("Stacionarne", c2.Type);// poprawnosc danych w modelu
            Assert.AreEqual(true, c2.Recruitment);// poprawnosc danych w modelu

            Assert.AreEqual(3, m1.Grades.Count); // sprawdzam czy są 3 oceny

            //sprawdzam czy na pierwszej pozycji na liscie kursów jest obiekt klasy
            //przechowujacej dane dla okreslonego kursu posiadajacy dane okreslonej osoby
            Assert.AreEqual("Adam", c1.CourseList.First().UserDataModel.Name);

        }
    }
}
